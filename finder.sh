#!/bin/bash

NEWLOC=`curl "https://api.github.com/repos/ViennaRSS/vienna-rss/releases/latest" | grep .tgz | tail -1 | awk -F'"' '{print $4}'`


if [ "x${NEWLOC}" != "x" ]; then
	echo ${NEWLOC}
fi