#!/bin/bash -ex

# CONFIG
prefix="Vienna"
suffix=""
munki_package_name="Vienna"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o vienna.tar.gz "${url}"

# unzip
pax -rz -f vienna.tar.gz

# copy app to build-root folder
mkdir -p build-root/Applications
cp -R Vienna.app build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" Vienna.app/Contents/Info.plist | sed 's/\ \:/./;s/\://'`

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} --scripts scripts app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

# Set version comparison key
/usr/libexec/PlistBuddy -c "Set :installs:0:version_comparison_key CFBundleVersion" app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}.pkg"
defaults write "${plist}" minimum_os_version "10.11.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${displayname}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}" 

# Make readable by humans
/usr/bin/plutil -convert xml1 app.plist
chmod a+r app.plist

# cleanup component plist to prevent upload
rm -rf Component-${munki_package_name}.plist

# Change filenames to suit
mv app.pkg   ${prefix}-${version}.pkg
mv app.plist ${prefix}-${version}.plist
